﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Payage_BE.DTOs;
using Payage_BE.Models;
using Payage_BE.Repositories.DBContext;
using Payage_BE.Services.Interfaces;

namespace Payage_BE.Controllers
{
    [Route("api/authorize")]
    [ApiController]
    public class PaymentTransactionsController : ControllerBase
    {
        private readonly PaymentTransactionContext _context;
        private readonly IPaymentTransactionService _service;

        public PaymentTransactionsController(PaymentTransactionContext context, IPaymentTransactionService service)
        {
            _context = context;
            _service = service;
        }

        //api to authenticate a payment transaction
        [HttpPost]
        public async Task<ActionResult<PaymentTransactionDTO>> PostPaymentTransaction(PaymentTransaction paymentTransaction)
        {
            try
            {
                PaymentTransactionDTO dto = await _service.Save(paymentTransaction);

                return CreatedAtAction(nameof(PostPaymentTransaction), new { id = paymentTransaction.ExposedId }, dto);
            }
            catch (TimeoutException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (WebException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //api to void api transaction
        [HttpPut("{id}/void")]
        public async Task<ActionResult<PaymentTransactionStatusChangeResponse>> Void(Guid Id, PaymentTransactionStatusChangeRequest req)
        {
            try
            {
                PaymentTransactionStatusChangeResponse resp = await _service.Void(Id, req);
                return Ok(resp);
            }
            catch(TimeoutException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (WebException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //api to capture payment transaction
        [HttpPut("{id}/capture")]
        public async Task<ActionResult<PaymentTransactionStatusChangeResponse>> Capture(Guid Id, PaymentTransactionStatusChangeRequest req)
        {
            try
            {
                PaymentTransactionStatusChangeResponse resp = await _service.Capture(Id,req);
                return Ok(resp);
            }
            catch (TimeoutException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (WebException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        //api to read payment transactions filtered by date and status, and paged. It's in POST 
        //to accept the filter object
        [HttpPost]
        [Route("read")]
        public  ActionResult<PaginatedResponse<List<PaymentTransactionDTO>>> GetPaymentTransaction(PaginatedRequest req)
        {
            try
            {
                PaginatedResponse<List<PaymentTransactionDTO>> resp = _service.GetPaymentTransactions(req);

                return Ok(resp);
            }
            catch (TimeoutException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (WebException te)
            {
                return StatusCode(500, te.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


    }
}
