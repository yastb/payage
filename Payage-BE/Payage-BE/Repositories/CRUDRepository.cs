﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore;
using Payage_BE.Repositories.Interfaces;
using System.Linq.Expressions;
using Payage_BE.Repositories.DBContext;

namespace Payage_BE.Repositories
{
    public class CRUDRepository<ID, E> : ReadOnlyRepository<ID, E>, ICRUDRepository<ID, E> where E : class
    {
        public CRUDRepository(PaymentTransactionContext Context) : base(Context)
        {
            this.Context = Context;
            this.Entities = this.Context.Set<E>();
        }

        virtual public async Task<E?> Create(E entity)
        {
            if (entity == null)
            {
                return null;
            }

            this.Entities.Add(entity);
            await this.Context.SaveChangesAsync();

            return entity;
        }

        virtual public async Task<IEnumerable<E>?> CreateRange(IEnumerable<E> entities)
        {
            if (entities == null)
            {
                return null;
            }

            Entities.AddRange(entities);
            await Context.SaveChangesAsync();

            return entities;
        }

        virtual public async Task<bool> Update(E entity)
        {
            EntityEntry entry = this.Context.Entry(entity);

            if (entry.State == EntityState.Detached)
            {
                this.Entities.Attach(entity);
            }

            entry.State = EntityState.Modified;

            await this.Context.SaveChangesAsync();
            return true;
        }

        virtual public async Task<bool> Delete(E entity)
        {
            EntityEntry entry = this.Context.Entry(entity);

            if (entry.State != EntityState.Deleted)
            {
                entry.State = EntityState.Deleted;
            }

            else
            {
                this.Entities.Attach(entity);
                this.Entities.Remove(entity);
            }

            await this.Context.SaveChangesAsync();
            return true;
        }

        virtual public async Task<bool> Delete(ID id)
        {
            E? entity = Find(id);

            if (entity != null)
            {
                return await Delete(entity);
            }

            return false;
        }

        virtual public async Task<bool> Delete(Expression<Func<E, bool>> predicate)
        {
            var entitiesToDelete = Entities.Where(predicate);

            if (entitiesToDelete != null && entitiesToDelete.Any())
            {
                Context.RemoveRange(entitiesToDelete);
            }
            await Context.SaveChangesAsync();
            return true;
        }
    }
}
