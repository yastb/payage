﻿using Payage_BE.Models;
using Payage_BE.Repositories.DBContext;

namespace Payage_BE.Repositories
{
    public class PaymentTransactionRepository : CRUDRepository<Guid, PaymentTransaction>
    {
        public PaymentTransactionRepository(PaymentTransactionContext Context) : base(Context)
        {
        }
    }
}
