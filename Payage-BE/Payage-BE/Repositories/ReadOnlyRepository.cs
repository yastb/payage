﻿using Microsoft.EntityFrameworkCore;
using Payage_BE.Repositories.DBContext;
using Payage_BE.Repositories.Interfaces;
using System.Linq.Expressions;

namespace Payage_BE.Repositories
{
    public abstract class ReadOnlyRepository<ID, E> : IReadOnlyRepository<ID,E> where E:class
    {
        protected PaymentTransactionContext Context;
        protected DbSet<E> Entities;

        public ReadOnlyRepository(PaymentTransactionContext Context)
        {
            this.Context = Context;
            this.Entities = this.Context.Set<E>();
        }

        public void Dispose()
        {
            this.Context.Dispose();
        }

        public IQueryable<E> Filter(Expression<Func<E, bool>> predicate)
        {
            return this.Entities.Where(predicate).AsQueryable<E>();
        }

        public IQueryable<E> Find()
        {
            return this.Entities.AsQueryable();
        }

        public E? Find(System.Linq.Expressions.Expression<Func<E, bool>> predicate)
        {
            return Filter(predicate).FirstOrDefault();
        }

        public E? Find(ID id)
        {
            E? entity = this.Entities.Find(id);

            return entity;
        }

    }
}
