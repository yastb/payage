﻿using System.Linq.Expressions;

namespace Payage_BE.Repositories.Interfaces
{
    public interface IReadOnlyRepository<ID, E> : IDisposable
    {
        IQueryable<E> Find();

        E? Find(Expression<Func<E, bool>> predicate);

        IQueryable<E> Filter(Expression<Func<E, bool>> predicate);

        E? Find(ID id);
    }
}
