﻿using System.Linq.Expressions;

namespace Payage_BE.Repositories.Interfaces
{
    public interface ICRUDRepository<ID, E> : IReadOnlyRepository<ID, E>
    {
        Task<E?> Create(E entity);

        Task<IEnumerable<E>?> CreateRange(IEnumerable<E> entities);

        Task<bool> Update(E entity);

        Task<bool> Delete(E entity);

        Task<bool> Delete(ID id);

        Task<bool> Delete(Expression<Func<E, bool>> predicate);

    }
}
