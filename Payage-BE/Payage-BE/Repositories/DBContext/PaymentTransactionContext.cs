﻿using Microsoft.EntityFrameworkCore;
using Payage_BE.Models;

namespace Payage_BE.Repositories.DBContext
{
    public class PaymentTransactionContext : DbContext
    {
        public PaymentTransactionContext(DbContextOptions<PaymentTransactionContext> options)
            : base(options)
        {
        }

        public DbSet<PaymentTransaction> PaymentTransactions { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var decimalProps = modelBuilder.Model
            .GetEntityTypes()
            .SelectMany(t => t.GetProperties())
            .Where(p => (Nullable.GetUnderlyingType(p.ClrType) ?? p.ClrType) == typeof(decimal));

            foreach (var property in decimalProps)
            {
                property.SetPrecision(18);
                property.SetScale(2);
            }
        }
    }
}
