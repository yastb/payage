﻿using Payage_BE.DTOs;
using Payage_BE.Models;
using Payage_BE.Repositories.Interfaces;
using Payage_BE.Services.Interfaces;

namespace Payage_BE.Services
{
    public class PaymentTransactionService: IPaymentTransactionService
    {
        private readonly ICRUDRepository<Guid, PaymentTransaction> _repository;

        public PaymentTransactionService(ICRUDRepository<Guid, PaymentTransaction> repository)
        {
            _repository = repository;   
        }

        private string hideCardNumber(string cardNumber)
        {
            if (String.IsNullOrWhiteSpace(cardNumber))
                return "";
            return cardNumber.Substring(0, 6) + "******" + cardNumber.Substring(cardNumber.Length - 4);
        }

        private PaymentTransactionDTO toDTO(PaymentTransaction pt)
        {
            return new PaymentTransactionDTO{
                ExposedId = pt.ExposedId,
                Amount = pt.Amount, 
                CardholderNumber = hideCardNumber(pt.CardholderNumber), //mettere ****
                Currency = pt.Currency,
                ExpirationMonth = pt.ExpirationMonth,
                CVV = pt.CVV,
                ExpirationYear = pt.ExpirationYear,
                HolderName = pt.HolderName,
                OrderReference = pt.OrderReference,
                Status = pt.Status,
                AuthenticatedAt = pt.AuthenticatedAt
            };
        }

        public async Task<PaymentTransactionDTO> Save(PaymentTransaction pt)
        {
            if(pt == null)
            {
                throw new Exception("PaymentTransaction cannot be null");
            }
            else
            {
                if(pt.ExpirationMonth > 12 || pt.ExpirationMonth < 1)
                {
                    throw new Exception("The expiration month you passed is not a valid month");
                }
                DateTime now = DateTime.Today;
                if(now.Year > pt.ExpirationYear || (now.Year == pt.ExpirationYear && now.Month > pt.ExpirationMonth))
                {
                    throw new Exception("Your card is expired");
                }
            }
            pt.ExposedId = Guid.NewGuid();
            pt.AuthenticatedAt = DateTime.Now;
            await _repository.Create(pt);
            return toDTO(pt);
        }

        public async Task<PaymentTransactionStatusChangeResponse> Void(Guid Id, PaymentTransactionStatusChangeRequest req)
        {
            if(req == null)
            {
                throw new Exception("Request cannot be null");
            }
            else if(Id == Guid.Empty || String.IsNullOrWhiteSpace(req.OrderReference))
            {
                throw new Exception("Id and Order reference are both mandatory");
            }
            else
            {
                PaymentTransaction? pt = _repository.Find(x => x.ExposedId == Id && x.OrderReference == req.OrderReference);
                if (pt == null)
                {
                    throw new Exception("No payment transaction found for Id and Order reference sent");
                }
                else if (pt.Status != PaymentTransaction.StatusEnum.Authorized)
                {
                    throw new Exception("The requested transaction is not authorized");
                }
                pt.Status = PaymentTransaction.StatusEnum.Voided;
                await _repository.Update(pt);
                PaymentTransactionStatusChangeResponse resp = new PaymentTransactionStatusChangeResponse();
                resp.Status = PaymentTransaction.StatusEnum.Voided;
                resp.Id = Id;
                return resp;

            }
        }

        public async Task<PaymentTransactionStatusChangeResponse> Capture(Guid Id, PaymentTransactionStatusChangeRequest req)
        {
            if (req == null)
            {
                throw new Exception("Request cannot be null");
            }
            else if (Id == Guid.Empty || String.IsNullOrWhiteSpace(req.OrderReference))
            {
                throw new Exception("Id and Order reference are both mandatory");
            }
            else
            {
                PaymentTransaction? pt = _repository.Find(x => x.ExposedId == Id && x.OrderReference == req.OrderReference);
                if (pt == null)
                {
                    throw new Exception("No payment transaction found for Id and Order reference sent");
                }
                else if(pt.Status != PaymentTransaction.StatusEnum.Authorized)
                {
                    throw new Exception("The requested transaction is not authorized");
                }
                pt.Status = PaymentTransaction.StatusEnum.Captured;
                await _repository.Update(pt);
                PaymentTransactionStatusChangeResponse resp = new PaymentTransactionStatusChangeResponse();
                resp.Status = PaymentTransaction.StatusEnum.Captured;
                resp.Id = Id;
                return resp;

            }
        }

        public PaginatedResponse<List<PaymentTransactionDTO>> GetPaymentTransactions(PaginatedRequest req)
        {
            if(req == null)
            {
                req = new PaginatedRequest();
            }
            if(req.DateTo != null)
            {
                req.DateTo = req.DateTo.Value.AddDays(1).AddSeconds(-1);
            }
            PaginatedResponse<List<PaymentTransactionDTO>> response = new PaginatedResponse<List<PaymentTransactionDTO>>();
            List<PaymentTransaction> pts = _repository.Find().ToList();
            if(req.DateFrom != null)
            {
                pts = pts.Where(x => x.AuthenticatedAt!= null && x.AuthenticatedAt.Value.CompareTo(req.DateFrom) >= 0).ToList();
            }
            if (req.DateTo != null)
            {
                pts = pts.Where(x => x.AuthenticatedAt != null && x.AuthenticatedAt.Value.CompareTo(req.DateTo) <= 0).ToList();
            }
            if (req.Status != null)
            {
                pts = pts.Where(x => (int)x.Status == req.Status).ToList();
            }
            int count = pts.Count;
            pts = pts.Skip((req.Page - 1) * req.ItemsPerPage).Take(req.ItemsPerPage).ToList();
            List<PaymentTransactionDTO> dtos = new List<PaymentTransactionDTO>();
            foreach(PaymentTransaction p in pts)
            {
                dtos.Add(toDTO(p));
            }
            response.Response = dtos;
            response.Page = req.Page;
            response.ItemsPerPage = req.ItemsPerPage;
            response.TotalCount = count;
            return response;
        }
    }
}
