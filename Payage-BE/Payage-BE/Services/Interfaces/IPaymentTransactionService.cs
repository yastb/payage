﻿using Payage_BE.DTOs;
using Payage_BE.Models;

namespace Payage_BE.Services.Interfaces
{
    public interface IPaymentTransactionService
    {
        Task<PaymentTransactionDTO> Save(PaymentTransaction pt);
        Task<PaymentTransactionStatusChangeResponse> Void(Guid Id,PaymentTransactionStatusChangeRequest req);
        Task<PaymentTransactionStatusChangeResponse> Capture(Guid Id, PaymentTransactionStatusChangeRequest req);
        PaginatedResponse<List<PaymentTransactionDTO>> GetPaymentTransactions(PaginatedRequest req);
    }
}
