﻿namespace Payage_BE.DTOs
{
    public class PaginatedRequest
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }  
        public int? Status { get; set; }
        public int Page { get; set; } = 1;
        public int ItemsPerPage { get; set; } = 5;

    }
}
