﻿namespace Payage_BE.DTOs
{
    public class PaginatedResponse<T>
    {
        public T? Response { get;  set; }
        public int Page { get; set; } = 1;
        public int ItemsPerPage { get; set; } = 10;
        public int TotalCount { get; set; } = 0;

    }
}
