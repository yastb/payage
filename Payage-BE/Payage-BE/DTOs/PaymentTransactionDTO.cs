﻿using static Payage_BE.Models.PaymentTransaction;
using System.ComponentModel.DataAnnotations;

namespace Payage_BE.DTOs
{
    //dto used to not expose database id
    public class PaymentTransactionDTO
    {
        public Guid? ExposedId { get; set; }
        public decimal Amount { get; set; } 
        public string Currency { get; set; } = ""; 
        public string CardholderNumber { get; set; } = ""; 
        public string HolderName { get; set; } = ""; 
        public int ExpirationMonth { get; set; } 
        public int ExpirationYear { get; set; } 
        public int CVV { get; set; } 
        public string OrderReference { get; set; } = "";
        public StatusEnum Status { get; set; }
        public DateTime? AuthenticatedAt { get; set; }
    }
}
