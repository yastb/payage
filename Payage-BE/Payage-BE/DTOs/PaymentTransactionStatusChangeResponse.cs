﻿using static Payage_BE.Models.PaymentTransaction;

namespace Payage_BE.DTOs
{
    public class PaymentTransactionStatusChangeResponse
    {
        public Guid Id { get; set; }
        public StatusEnum Status { get; set; }
    }
}
