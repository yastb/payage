﻿using Payage_BE.Models;
using Payage_BE.Repositories.DBContext;
using Payage_BE.Repositories.Interfaces;
using Payage_BE.Repositories;
using Payage_BE.Services.Interfaces;
using Payage_BE.Services;
using Microsoft.EntityFrameworkCore;

namespace Payage_BE
{
    public class Startup
    {
        public IConfiguration configRoot
        {
            get;
        }
        public Startup(IConfiguration configuration)
        {
            configRoot = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();

            services.AddDbContext<PaymentTransactionContext>(
                options =>
                            options.UseSqlServer(configRoot["DatabaseConnection"]));

            services.AddScoped<IPaymentTransactionService, PaymentTransactionService>();
            services.AddScoped<ICRUDRepository<Guid, PaymentTransaction>, PaymentTransactionRepository>();

            services.AddEndpointsApiExplorer();
        }
        public void Configure(WebApplication app)
        {
            if (app.Environment.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
