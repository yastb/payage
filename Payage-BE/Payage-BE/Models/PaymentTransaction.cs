﻿using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography.Xml;
using System.Xml.Linq;

namespace Payage_BE.Models
{
    public class PaymentTransaction
    {
        public enum StatusEnum
        {
            Authorized,Captured, Voided
    }
        [Key]
        public int Id { get; set; }
        public Guid? ExposedId { get; set; }
        public decimal Amount { get; set; } 
        [StringLength(3, ErrorMessage = "Currency value cannot exceed 3 characters. ")]
        public string Currency { get; set; } = ""; 
        public string CardholderNumber { get; set; } = ""; 
        public string HolderName { get; set; } = ""; 
        public int ExpirationMonth { get; set; } 
        public int ExpirationYear { get; set; } 
        public int CVV { get; set; } 
        [StringLength(50, ErrorMessage = "OrderReference value cannot exceed 50 characters. ")]
        public string OrderReference { get; set; } = "";
        public StatusEnum Status { get; set; }
        public DateTime? AuthenticatedAt { get; set; } = DateTime.Now;
    }
}
