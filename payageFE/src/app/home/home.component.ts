import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { PaginatedRequest } from '../../classes/PaginatedRequest';
import { PaymentTransaction } from '../../classes/PaymentTransaction';
import { PaymentTransactionStatusChangeRequest } from '../../classes/PaymentTransactionStatusChangeRequest';
import { Service } from '../../services/service.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { StatusEnum } from '../../classes/StatusEnum';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Payment transactions';
  paymentTransactions: Array<PaymentTransaction> | null = [];
  newPT: PaymentTransaction = new PaymentTransaction();
  selectedRow: PaymentTransaction | null = null;
  paginatedRequest: PaginatedRequest;
  modalRefVoid: BsModalRef = new BsModalRef();
  modalRefCapture: BsModalRef = new BsModalRef();
  modalRefAdd: BsModalRef = new BsModalRef();
  selectedExposedId: string = '';
  selectedOrderReference: string = '';
  lastPage: number = 1;
  elementsString: string = '';
  options = [
    { name: "All", value: null },
    { name: "Authorized", value: 0 },
    { name: "Captured", value: 1 },
    { name: "Voided", value: 2 }
  ]

  constructor(private service: Service, private modalService: BsModalService, private toastr: ToastrService) {
    this.paginatedRequest = new PaginatedRequest();
    this.newPT.status = StatusEnum.Authorized;
  }

  ngOnInit() {
    this.search();
  }

  changePage(n: number) {
    this.paginatedRequest.Page = n;
    this.search();
  }

  search() {
    this.service.read(this.paginatedRequest).subscribe(
      {
        next: (resp) => {
          this.paymentTransactions = resp.response;
          this.lastPage = Math.ceil(resp.totalCount / resp.itemsPerPage);
          if (this.lastPage == 0) {
            this.lastPage = 1;
          }
          if (this.paginatedRequest.Page > this.lastPage) {
            this.paginatedRequest.Page = 1;
          }
          this.elementsString = "Elements " + ((this.paginatedRequest.Page - 1) * resp.itemsPerPage + 1) + "-" + Math.min(this.paginatedRequest.Page * resp.itemsPerPage, resp.totalCount) + " of " + resp.totalCount;
        },
        error: (e) =>{ 
          this.toastr.error("An error occurred: "+e.message);
          console.error(e);},
        complete: () => console.info('complete')
      });
  }

  showVoidModal(template: TemplateRef<any>, exposedId: string, orderReference: string) {
    this.selectedExposedId = exposedId;
    this.selectedOrderReference = orderReference;
    this.modalRefVoid = this.modalService.show(template, {
      class: 'modal-lg',
      backdrop: 'static'
    });
  }

  showCaptureModal(template: TemplateRef<any>, exposedId: string, orderReference: string) {
    this.selectedExposedId = exposedId;
    this.selectedOrderReference = orderReference;
    this.modalRefCapture = this.modalService.show(template, {
      class: 'modal-lg',
      backdrop: 'static'
    });
  }

  showAddModal(template: TemplateRef<any>) {
    this.modalRefAdd = this.modalService.show(template, {
      class: 'modal-lg',
      backdrop: 'static'
    });
  }

  capture() {
    let scr: PaymentTransactionStatusChangeRequest = new PaymentTransactionStatusChangeRequest();
    scr.OrderReference = this.selectedOrderReference;
    this.service.capture(this.selectedExposedId, scr).subscribe({
      next: (resp) => {
        this.toastr.success("Payment transaction captured correctly");
        this.search();
      },
      error: (e) => {
        this.toastr.error("An error occurred: "+e.error);
      }
    });
  }

  void() {
    let scr: PaymentTransactionStatusChangeRequest = new PaymentTransactionStatusChangeRequest();
    scr.OrderReference = this.selectedOrderReference;
    this.service.void(this.selectedExposedId, scr).subscribe({
      next: (resp) => {
        this.toastr.success("Payment transaction voided correctly");
        this.search();
      },
      error: (e) => {
        this.toastr.error("An error occurred: "+e.error);
      }
    });
  }

  add(){
    if(!this.newPT.checkCompleteness()){
      this.toastr.error("All fields are mandatory");
      return;
    }
    this.newPT.orderReference = this.newPT.cardholderNumber.substring(0,6)+"_"+new Date().getTime();
    this.service.authorize(this.newPT).subscribe({
      next: (resp) => {
        this.toastr.success("Payment transaction authorized correctly");
        this.search();
      },
      error: (e) => {
        this.toastr.error("An error occurred: "+e.error);
        //console.error(e)
      }
    })
  }


}
