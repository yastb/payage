export const environment = {
  production: true,
  basepath: 'https://localhost:7275/api/',
  api: {
    authorize: {
      method: 'POST',
      url: 'authorize'
    },
    void:{
      method: 'PUT',
      url: 'authorize/{0}/void'
    },
    capture:{
      method: 'PUT',
      url: 'authorize/{0}/capture'
    },
    read:{
      method: 'POST',
      url: 'authorize/read'
    }
  }
};