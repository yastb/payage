// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  basepath: 'https://localhost:7275/api/',
  api: {
    authorize: {
      method: 'POST',
      url: 'authorize'
    },
    void:{
      method: 'PUT',
      url: 'authorize/{0}/void'
    },
    capture:{
      method: 'PUT',
      url: 'authorize/{0}/capture'
    },
    read:{
      method: 'POST',
      url: 'authorize/read'
    }
  }
};
