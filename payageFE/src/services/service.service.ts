import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PaymentTransaction } from '../classes/PaymentTransaction';
import { PaginatedResponse } from '../classes/PaginatedResponse';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { PaginatedRequest } from '../classes/PaginatedRequest';
import { PaymentTransactionStatusChangeRequest } from '../classes/PaymentTransactionStatusChangeRequest';
import { PaymentTransactionStatusChangeResponse } from '../classes/PaymentTransactionStatusChangeResponse';

@Injectable({
  providedIn: 'root'
})
export class Service {

  constructor(private http: HttpClient) { }

  public read(req: PaginatedRequest): Observable<PaginatedResponse<Array<PaymentTransaction>|null>> {
    return this.http.post<PaginatedResponse<Array<PaymentTransaction>|null>>(environment.basepath + environment.api.read.url, req);
  }

  public authorize(pt: PaymentTransaction): Observable<PaymentTransaction>{
    return this.http.post<PaymentTransaction>(environment.basepath+environment.api.authorize.url, pt);
  }

  public void(exposedId:string,scr:PaymentTransactionStatusChangeRequest): Observable<PaymentTransactionStatusChangeResponse>{
    return this.http.put<PaymentTransactionStatusChangeResponse>(environment.basepath+environment.api.void.url.replace("{0}",exposedId), scr);
  }

  public capture(exposedId:string,scr:PaymentTransactionStatusChangeRequest): Observable<PaymentTransactionStatusChangeResponse>{
    return this.http.put<PaymentTransactionStatusChangeResponse>(environment.basepath+environment.api.capture.url.replace("{0}",exposedId), scr);
  }
}
