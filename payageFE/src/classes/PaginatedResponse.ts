export class PaginatedResponse<Type> {
    response: Type|null = null;
    totalCount: number = 0;
    page: number = 1;
    itemsPerPage: number= 10;
}