import { StatusEnum } from "./StatusEnum";

export class PaymentTransaction {
    exposedId: string|null = null;
    amount: number = 0; 
    currency: string = ''; 
    cardholderNumber: string = ''; 
    holderName: string = '';
    expirationMonth: number = 0;
    expirationYear: number = 0;
    cvv: number = 0;
    orderReference: string = '';
    status: StatusEnum|null = null;
    authenticatedAt: Date|null = null;

    checkCompleteness(){
        return this.amount && this.currency && this.cardholderNumber && this.holderName && this.expirationMonth 
            && this.expirationYear && this.cvv; 
    }
}