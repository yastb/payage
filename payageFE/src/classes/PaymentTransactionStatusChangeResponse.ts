import { StatusEnum } from "./StatusEnum";

export class PaymentTransactionStatusChangeResponse {

    id: string = '';
    status: StatusEnum|null = null;
}