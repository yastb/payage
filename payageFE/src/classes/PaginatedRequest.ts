export class PaginatedRequest {

    DateFrom: Date|null = null;
    DateTo: Date|null = null;
    Status: number|null = null;
    Page: number = 1;
    ItemsPerPage: number= 5;
}